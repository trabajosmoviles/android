package com.example.sofi.act9;

import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    IntentFilter filter = new IntentFilter();
    MyReceiver myReceiver = new MyReceiver();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        filter.addAction("com.example.sofi.act9");
        registerReceiver(myReceiver,filter);
    }

    public void Emision(View view){
        //Generamos el objeto tipo Intent
        Intent intent = new Intent();
        //Añadimos la accion de nuestro paquete de nuestra app
        intent.setAction("com.example.sofi.act9");
        //Añadimos la bandera de emision
        intent.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        //Enviamos la emision
        sendBroadcast(intent);
    }
}

