package com.example.sofi.act9;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class MyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        //Mensaje con toast de recibida de la emision
        Toast.makeText(context,"Transmision Recibida", Toast.LENGTH_SHORT).show();
    }
}
