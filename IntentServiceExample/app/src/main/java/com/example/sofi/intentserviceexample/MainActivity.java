package com.example.sofi.intentserviceexample;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.content.ServiceConnection;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = new Intent(this,MyService2.class);
        bindService(intent,serviceConnection, Context.BIND_AUTO_CREATE);

    }


    public void InitAction(View view) {
        Intent intent = new Intent(this,MyIntentService.class);
        startService(intent);
    }

    public  void InitAction2(View view){
        Intent intent = new Intent(this,MyService.class);
        startService(intent);
    }

    private MyService2 myService2;
    private boolean isBounded = false;

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder){
            MyService2.MyLocalBinder myLocalBinder = (MyService2.MyLocalBinder) iBinder;
            myService2 = myLocalBinder.getService();
            isBounded = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName){
            isBounded = false;
        }
    };

    public  void InitAction3(View view){
        String time = myService2.getCurrentTime();
        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText(time);
    }



}
