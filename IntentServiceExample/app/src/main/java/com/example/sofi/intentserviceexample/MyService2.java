package com.example.sofi.intentserviceexample;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MyService2 extends Service {

    public class MyLocalBinder extends Binder{
        MyService2 getService(){
            return MyService2.this;
        }
    }

    private final IBinder myBinder = new MyLocalBinder();

    @Nullable
    @Override

    public IBinder onBind(Intent intent){
        return myBinder;
    }

    public String getCurrentTime(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss",Locale.US);
        return simpleDateFormat.format(new Date());
    }


}
