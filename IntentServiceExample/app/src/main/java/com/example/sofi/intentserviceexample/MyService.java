package com.example.sofi.intentserviceexample;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.lang.reflect.Type;

public class MyService extends Service {

    private static final String TYPE = "MyService";
    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        Log.i(TYPE,"Iniciamos el servicio");

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                long startTime = System.currentTimeMillis();
                long finalTime = startTime + 200000;

                while (System.currentTimeMillis() < finalTime){
                    synchronized (this){
                        try{
                            wait(finalTime - System.currentTimeMillis());
                        } catch (Exception e) {

                        }
                    }
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy(){
        Log.i(TYPE,"metodo onDestroy() llamado");
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent){
        return null;
    }



}
