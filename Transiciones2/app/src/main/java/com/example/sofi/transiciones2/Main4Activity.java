package com.example.sofi.transiciones2;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.ArcMotion;
import android.transition.ChangeBounds;
import android.transition.ChangeImageTransform;
import android.transition.Explode;
import android.transition.Scene;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.transition.TransitionSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Main4Activity extends AppCompatActivity {
    private Context mContext;
    private Activity mActivity;

    private static final String TAG = MainActivity.class.getSimpleName();

    private CoordinatorLayout mCLayout;
    private ImageView mImageView;
    private int mHeight = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);

        // Get the application context
        mContext = getApplicationContext();
        mActivity = Main4Activity.this;

        // Get the widget reference from XML layout
        mCLayout = (CoordinatorLayout) findViewById(R.id.coordinator_layout);
        mImageView = (ImageView) findViewById(R.id.iv);

        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*
                    ChangeImageTransform
                        This Transition captures an ImageView's matrix before and after the scene
                        change and animates it during the transition.

                        In combination with ChangeBounds, ChangeImageTransform allows ImageViews
                        that change size, shape, or ImageView.ScaleType to animate contents smoothly.
                */

                // Initialize a new ChangeImageTransform instance
                ChangeImageTransform imageTransform = new ChangeImageTransform();
                // Set the duration for image transform
                imageTransform.setDuration(1000);

                /*
                    ChangeBounds
                        This transition captures the layout bounds of target views before and after
                        the scene change and animates those changes during the transition.
                */

                // Initialize a new ChangeBounds instance
                ChangeBounds changeBounds = new ChangeBounds();
                // Set the change bounds duration
                changeBounds.setDuration(1000);

                // Initialize new TransitionSet instance
                TransitionSet set = new TransitionSet();

                // Specify the transition set transitions ordering
                set.setOrdering(TransitionSet.ORDERING_TOGETHER);
                //set.setOrdering(TransitionSet.ORDERING_SEQUENTIAL);

                // Add the ChangeImageTransform transition to transition set
                // Animate image scale type
                set.addTransition(imageTransform);

                // Add the ChangeBounds transition to transition set
                // ChangeBounds and ChangeImageTransform works together
                set.addTransition(new ChangeBounds());

                // Finally, go for transition
                TransitionManager.go(new Scene(mCLayout),set);

                // Toggle the image scale type
                toggleImageScaleType(mImageView);

                // Get the image view initial height
                if(mHeight == 0){
                    mHeight = mImageView.getHeight();
                }

                // Toggle the image view height
                toggleSize(mImageView, mHeight);
            }
        });
    }

    // Custom method to toggle view size
    protected void toggleSize(View v,int initHeight){
        if(v.getHeight() == initHeight){
            viewMatchLayoutSize(v);
        }else {
            toggleViewHeight(v,initHeight);
        }
    }

    // Custom method to toggle image scaling type
    protected void toggleImageScaleType(ImageView v){
        if(v.getScaleType() == ImageView.ScaleType.CENTER){
            v.setScaleType(ImageView.ScaleType.CENTER_CROP);
            Toast.makeText(mContext,"Center crop",Toast.LENGTH_LONG).show();
        }else {
            v.setScaleType(ImageView.ScaleType.CENTER);
            Toast.makeText(mContext,"Center",Toast.LENGTH_LONG).show();
        }
    }

    // Custom method to set a view width and height as container
    protected void viewMatchLayoutSize(View v){
        ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) v.getLayoutParams();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height =ViewGroup.LayoutParams.MATCH_PARENT;
        v.setLayoutParams(params);
    }

    // Custom method to change view height
    protected void toggleViewHeight(View v, int height){
        ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) v.getLayoutParams();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = height;
        v.setLayoutParams(params);
    }
}
