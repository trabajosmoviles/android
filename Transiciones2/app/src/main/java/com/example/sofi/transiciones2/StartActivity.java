package com.example.sofi.transiciones2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
    }


    public void startBBAcivity(View view){

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

    }


    public void startTransAcivity(View view){

        Intent intent = new Intent(this, Main2Activity.class);
        startActivity(intent);

    }

    public void startBotonTextoActivity(View view){

        Intent intent = new Intent(this, Main3Activity.class);
        startActivity(intent);

    }

    public void startExplodeActivity(View view){

        Intent intent = new Intent(this, Main4Activity.class);
        startActivity(intent);

    }
}
