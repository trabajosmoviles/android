package com.example.sofi.multimedia;

import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;

public class grabarAudio extends AppCompatActivity {

    Button btnPlay,btnStop,btnRecord;
    private MediaRecorder myAudioRecorder;
    private String outputFile = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.grabar_audio);

        btnRecord = (Button) findViewById(R.id.btnRecordAudio);
        btnPlay = (Button) findViewById(R.id.btnPlayAudio);
        btnStop = (Button) findViewById(R.id.btnPauseAudio);

        btnStop.setEnabled(false);
        btnPlay.setEnabled(false);

        outputFile = Environment.getExternalStorageDirectory().getAbsolutePath() + "/recording.3gp";

        myAudioRecorder = new MediaRecorder();
        myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        myAudioRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        myAudioRecorder.setOutputFile(outputFile);

        btnRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    myAudioRecorder.prepare();
                    myAudioRecorder.start();
                }catch (IllegalStateException e){
                    e.printStackTrace();
                }catch(IOException e){
                    e.printStackTrace();
                }

                btnRecord.setEnabled(true);
                btnStop.setEnabled(true);

                Toast.makeText(grabarAudio.this,"Empezo Grabacion",Toast.LENGTH_SHORT).show();
            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myAudioRecorder.stop();
                myAudioRecorder.release();
                myAudioRecorder = null;

                btnStop.setEnabled(false);
                btnPlay.setEnabled(true);

                Toast.makeText(grabarAudio.this,"Grabacion Exitosa",Toast.LENGTH_SHORT).show();
            }
        });

        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MediaPlayer m = new MediaPlayer();

                try{
                    m.setDataSource(outputFile);
                }catch(IOException e){
                    e.printStackTrace();
                }

                try{
                    m.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }

                m.start();

                Toast.makeText(grabarAudio.this,"Reproduciendo Audio",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
