package com.example.sofi.multimedia;

import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class tomarFoto extends AppCompatActivity {

    Button btnTakePic;
    ImageView imgPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tomar_foto);

        btnTakePic = (Button) findViewById(R.id.btnTomaFoto);
        imgPhoto = (ImageView) findViewById(R.id.verfotoimg);

        btnTakePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,0);
            }
        });


        }

        protected void onActivityResult(int requestcode, int resultCode, Intent data){
            super.onActivityResult(requestcode,resultCode,data);
            switch(requestcode){
                case 0 :
                    Bitmap bp = (Bitmap) data.getExtras().get("data");
                    imgPhoto.setImageBitmap(bp);
                    break;
            }
        }
    }

