package com.example.sofi.multimedia;

import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;

public class grabarVideo extends AppCompatActivity {

    Button btnPlay,btnStop,btnRecord;
    private MediaRecorder myVideoRecorder;
    private String outputFile = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.grabar_video);

        btnRecord = (Button) findViewById(R.id.btnRecordVideo);
        btnPlay = (Button) findViewById(R.id.btnPlayVideo);
        btnStop = (Button) findViewById(R.id.btnPauseVideo);

        btnStop.setEnabled(false);
        btnPlay.setEnabled(false);

        outputFile = Environment.getExternalStorageDirectory().getAbsolutePath() + "/MyVideo.mpeg4";

        myVideoRecorder = new MediaRecorder();
        myVideoRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        myVideoRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        myVideoRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264 );
        myVideoRecorder.setOutputFile(outputFile);

        btnRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    myVideoRecorder.prepare();
                    myVideoRecorder.start();
                }catch (IllegalStateException e){
                    e.printStackTrace();
                }catch(IOException e){
                    e.printStackTrace();
                }

                btnRecord.setEnabled(true);
                btnStop.setEnabled(true);

                Toast.makeText(grabarVideo.this,"Empezo Grabacion",Toast.LENGTH_SHORT).show();
            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myVideoRecorder.stop();
                myVideoRecorder.release();
                myVideoRecorder = null;

                btnStop.setEnabled(false);
                btnPlay.setEnabled(true);

                Toast.makeText(grabarVideo.this,"Grabacion Exitosa",Toast.LENGTH_SHORT).show();
            }
        });

        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MediaPlayer m = new MediaPlayer();

                try{
                    m.setDataSource(outputFile);
                }catch(IOException e){
                    e.printStackTrace();
                }

                try{
                    m.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }

                m.start();

                Toast.makeText(grabarVideo.this,"Reproduciendo Audio",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
