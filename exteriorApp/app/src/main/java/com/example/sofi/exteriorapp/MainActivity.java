package com.example.sofi.exteriorapp;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText editText;
    Button button;
    Button buttonWeb;
    Button buttonMaps;
    Button buttonCall;
    Button buttonCamara;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText = (EditText) findViewById(R.id.editText);
        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!editText.getText().toString().equals("")) {
                    GoToSecondActivity();
                }else{
                    Toast.makeText(MainActivity.this, "Ingrese un valor", Toast.LENGTH_SHORT).show();
                }
            }
        });
        buttonWeb=(Button)findViewById(R.id.buttonWiki);
        buttonWeb.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                Uri uri = Uri.parse("http://es.wikipedia.org/wiki/"+Uri.encode("Mexico",null));
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                 }
        });

        buttonMaps = (Button) findViewById(R.id.buttonMaps);
        buttonMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri gmmIntentUri=Uri.parse("geo:25.5862851,-100.2677858");
                Intent intent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                intent.setPackage("com.google.android.apps.maps");
                if(intent.resolveActivity(getPackageManager())!= null){
                    startActivity(intent);
                }else{
                    Toast.makeText(MainActivity.this, "Error al abrir las coordenadas", Toast.LENGTH_SHORT).show();
                }
            }
        });

        buttonCall = (Button) findViewById(R.id.buttonCall);
        buttonCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED){
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CALL_PHONE},1);
                }else{
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:81105200000"));
                    startActivity(intent);
                }
            }
        });

        buttonCamara = (Button) findViewById(R.id.buttonCamara);
        buttonCamara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                if(ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA},1);
                }else{
                    startActivity(intent);
                }
            }
        });
    }


    public void GoToSecondActivity(){
        Intent intent = new Intent(this, Main2Activity.class);
        intent.putExtra("name",editText.getText().toString());
        intent.putExtra("int",100);
        intent.putExtra("boolean", true);
        startActivity(intent);

    }
}
