package com.example.sofi.exteriorapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        TextView textView1= (TextView) findViewById(R.id.textView1);
        TextView textView2= (TextView) findViewById(R.id.textView2);
        CheckBox textView3= (CheckBox) findViewById(R.id.checkBox3);
        Intent intent = getIntent();
        String string = intent.getStringExtra("name");
        int value = intent.getIntExtra("int",1);
        boolean boolText = intent.getBooleanExtra("boolean",false);

        textView1.setText(string);
        textView2.setText(String.valueOf(value));
        textView3.setChecked(boolText);
    }
}
